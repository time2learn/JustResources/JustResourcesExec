// swift-tools-version:5.4
// Package: JustResourcesExec

import PackageDescription

let package = Package(
    name: "JustResourcesExec",
    platforms: [
        // specify each minimum deployment requirement,
        // otherwise the platform default minimum is used.
        .macOS(.v11), // .v10_14 Mojave, .v10_15 Catalina
    ],
    products: [
        // `Product` defines and make executables and libraries visible to other packages.
        .executable(
            name: "JustResourcesExec",
            targets: ["JustResourcesExec"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(
            url: "git@gitlab.com:time2learn/JustResources/JustResourcesDLib.git",
            .branch("master")
        ),
        .package(
            url: "git@gitlab.com:time2learn/JustResources/JustResourcesSLib.git",
            .branch("master")
        ),
    ],
    targets: [
        // Targets are the basic building blocks of a package. 
        // A target can define a module or a test suite.
        // Targets can depend on other targets in this package, 
        // and on products in packages this package depends on.
        .executableTarget(
            name: "JustResourcesExec",
            dependencies: ["JustResourcesDLib", "JustResourcesSLib"],
            resources: [.copy("Resources/"),]
        ),
        .testTarget(
            name: "JustResourcesExecTests",
            dependencies: ["JustResourcesExec"]
        ),
    ],
    // -- Optionally, specify the minimum language version --
    swiftLanguageVersions: [SwiftVersion.v5]
    //cLanguageStandard: CLanguageStandard.c11,
    //cxxLanguageStandard: CXXLanguageStandard.cxx14
)
